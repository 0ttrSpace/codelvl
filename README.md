## Level Up Code projects
What is it about and why should you care? Well, do you want a job in IT, Cyber Security, DevSsecOps, OSINT, or System Administration? Probably a yes if you are on this site.

YouTube, in it's ass-hole-infinite-wisdom, suggested a coding video to watch. Since I had just woken up and was still making my coffee I decided to click on it and see what generic advice I got from another coding youtuber. To my surpsise it gave some good adivce and I rewatched it several times. 

The video (by [Andy Sterkowitz](https://www.youtube.com/channel/UCZ9qFEC82qM6Pk-54Q4TVWA)) suggested that if you are trying to get a programing job that you fill your Github with some projects that follow his Recommended guidelines to show off your skill. Doing this while you learn can also show off your growth as a programer and that is something cool for you to see; maybe even nice for a Potential Employer to see. 

Since I am learning Python and Bash Scripting as part of my Cybersecurity journey I decided to take what Andy said, modify it a bit for my needs and complete three levels of programs and scripts. I stuck to what he suggests in his video pretty closely but added my spin to fit my goals and what I feel is acomplishable. When I say acomplishable I don't mean that I will stay in my comfortzone or stick with what I know only. I mean I am not going to clone Discord or Twitter. These are not things I want to do and if I do not want to do them, at the end of the day, I wont. 

I am going to consider projects before I take them on and if it's something I will ever use. If they do not challenge me or seem like a project I will ever use myself then there is just no reason for me to code it. I am not looking to get into the business of coding for clients or a company so I will never take such a project on (unless I am offered an insane amount of money). 

## Level One
Code eight simple (meaning not complex) programs and scripts. I say programs and scripts because I plan to do four Python projects and four Bash projects. Each language (Python/Bash) will have two projects with no focus and two with a cybersecurity focus.

Complete these programs should showcase basic understanding of Python and Bash; good understanding of clean code and syntax; and flow control statements.

#### Guidelines
- Less than 200 lines of code (shoot for less than 20 in bash)
- No libraries or frameworks used in the code
- Accomplish 1-3 tasks in each program 

#### Python:
- [x] Quick Note [general]
- [ ] Count Down [general]
- [ ] Dork site for documents [cyber]
- [ ] Whoami [cyber]

#### Bash:
- [ ] Clipboard modify [general]
- [ ] Change Wallpaper [general]
- [ ] Ping Change Notify [cyber]
- [ ] Change hosts file for CTF box use [cyber]

## Level Two
These programs should get more complex that Level One but still be pretty simple. Maybe a simple game or even webscraping is a good example of a Level Two script. I will be making four projects on this level, two for Python and two for Bash. Each Language will have one split focus again one focusing on Cybersecurity and the other can be fore fun or just a tool I might find useful. 

#### Guidelines
- Less than 500 lines of code
- Acomplish 3-5 tasks in the project
- Minimal use of libraies or frameworks

#### Python:
- [ ] General
- [ ] Cyber

#### Bash:
- [ ] General
- [ ] Cyber

## Level Three

These projects should be high in complexity and will likely use multiple libraries or frameworks. These projects will give you the chance to show off your problem solving AND your creativity. These level of Projects can be "clone" projects (i.e. make a LinkedIn, or Discord clone or Fake e-commerce webpage). 

For Bash I will suggest a script that sets up your full configuration for Arch Linux, Kali Linux, or does a full sweep of a CTF box where you are left with some possible vulnerability vectors. 

For Level Three Andy suggests one big project but because I am trying to cover Python and Bash I will be doing two. The Python project I am not sure about but the Bash project will be to setup a fresh insall of Arch Linux to my configurations and include my top used programs for Cybersecurity (from BalckArch or their git repos). Basically dotfiles but once I start working in Cybersecurity or participating in large scale CTFs then having the ability to spin up a VM and run a script that I can walk away from and come back to a setup I am used to, and like, is very helpful and benificial. 

#### Guidelines 
- Acomplish 10 or more tasks
- Use integrations (Databases, other programs, external APIs)
- Use multiple libraries and frameworks
- Have 500 or more lines of code (250 for Bash)

#### Python:
- [ ] Cyber

#### Bash: 
- [ ] Cyber
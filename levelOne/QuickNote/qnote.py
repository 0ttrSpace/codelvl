#
# Quick note taker python project by 0ttrSpace
# @0ttrSpace https://otterspace.org
#


# Setting day and time as the file name
from datetime import datetime
# from pathlib import Path # commented out for future additions, not needed in current build.

home = str(Path.home())
now = datetime.now()
fname = now.strftime("%Y-%m-%d_%H:%M")
print("filename will be", fname)

# get note
note = input("Enter quick note now:\n")
print("This is your note: \n" + note)

# Write to file
file = open(fname+".txt", "a")
file.write(note)
file.close()


osstr=$(uname)

while true
do 
if [[ "$osstr" == 'Darwin' ]]; then
	pbpaste > /tmp/copypasta
	vim /tmp/copypasta
elif [[ "$osstr" != "Darwin" ]]; then
	echo This is not MacOS!
	break
fi

echo Done editing? [y/n]
read ans

if [[ $ans == 'y' ]]; then
	pbcopy < /tmp/copypasta
	echo All done!
	break
elif [[ $ans == 'n' ]]; then
	echo Back to the begining!
fi
done

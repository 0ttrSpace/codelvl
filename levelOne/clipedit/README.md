## Copy Pasta

A bash script to allow a user to edit the content of their clipboard. Since I use MacOS and GNU/Linux on my computers I have it check what OS is running. Currently I only have it set to work if I am using MacOS, when I can I will set up the code for using it in a Linux enviroment. 

After testing the OS the script will use the appropiate terminal commands to take the content of the clipboard, save it to a file in /tmp and then open said file in vim. When you close Vim the script will ask if you are done editing, if yes it taked the content of the file and make it the content of your clipboard. 

Currently if you enter 'n' as your answer for 'Done editing' then you get a promt to restart the script. This is a temporary measure until I can figure out how to get the script to loop back to start. 

#### ToDo
 - [ ] Add Linux commands 
 - [x] Loop back if 'Done editing' is answered with no
